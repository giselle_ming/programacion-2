#include <stdio.h>
#include <stdlib.h>

struct nodo
{
    int numero;
    struct nodo *sig;
};

typedef struct nodo node;

node *crearCola(node *inicio)
{
    inicio = NULL;
    return inicio;
}

void imprimir (node *cola)
{
     node *auxiliar;
     auxiliar = cola;
     
     while (auxiliar != NULL)
     {
           printf("%d \n",auxiliar->numero);
           auxiliar = auxiliar->sig;
     }
     
     return;
}

node *encolar(int numeroAInsertar, node *inicio)
{
    node *nuevoNodo;
    node *aux;
    
    nuevoNodo = (node *) malloc(sizeof(node));
    
    nuevoNodo->numero = numeroAInsertar;
    nuevoNodo->sig = NULL;
    
    if (inicio == NULL)
    {
        inicio = nuevoNodo;
    }
    else
    {
        aux = inicio;
        
        while (aux->sig != NULL)
        {
            aux = aux->sig;
        }
            
        aux->sig = nuevoNodo;
    }
    
    return inicio;
}

node *desencolar (node *apuntador)
{
     if (apuntador != NULL)
     {
                   
                   node *auxiliar;
                   
                   auxiliar = apuntador;
                   
                   apuntador = apuntador->sig;
                   
                   free (auxiliar);
     }
     
     return apuntador;
     
}
 

int main()
{
    
    node *cola;
    
    cola = crearCola (cola);
    
    cola = encolar(3, cola);
    cola = encolar(4, cola);
    cola = encolar(8, cola);
    cola = encolar(3, cola);
    cola = encolar(2, cola);
    
    imprimir(cola);
    
    printf("////// \n");
    
    cola = desencolar(cola);
    cola = desencolar(cola);
    cola = desencolar(cola);
    cola = desencolar(cola);
    cola = desencolar(cola);
    imprimir(cola);
    
    
  
  system("PAUSE");	
  return 0;
}
