#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#define _getch getch
#define nombreDeArchivo "Elementos.txt"

typedef enum {IZQUIERDA, CENTRO, DERECHA} Sentido;

typedef struct AVLArbol
{
	struct AVLArbol *izq;
	struct AVLArbol *der;
	int entero;

} *Arbol;

Arbol arbolInsertar       (Arbol nodo, int entero);
Arbol arbolQuitarYRotar   (Arbol nodo, int entero, Sentido rotacion);
void  arbolPreorden       (Arbol raiz, Arbol nodo);
void  arbolInorden        (Arbol raiz, Arbol nodo);
void  arbolPostorden      (Arbol raiz, Arbol nodo);
int   arbolAltura         (Arbol nodo);
Arbol arbolBalancear      (Arbol nodo);
int   arbolNodos          (Arbol nodo);
int   arbolCompleto       (Arbol nodo);
int   arbolBalanceado     (Arbol nodo);
void  buscarArbol         (Arbol nodo, int entero);
void  arbolInsertarFile   ();

	
	FILE *archivo;
    Arbol raiz = NULL;
    
int main ()
{

	int entero, i, opcion;
	
	do {
		system ("cls");
		printf (
			"Bienvendo a El Balanceador - Version UCAB\n\n\n"
			"MENU\n\n"
			"1.- Insertar entero\n"
	     	"2.- Insertar elementos desde un archivo\n"
			"3.- Listado en preorden\n"
			"4.- Listado en inorden\n"
			"5.- Listado en postorden\n"
			"6.- Balancear\n"
			"7.- Consultar Arbol\n"
			"8.- Buscar\n"
			"9.- Salir.\n\n"
			"Seleccione una opcion: ");
		fflush (stdout);
		do {
			opcion = _getch ();
		} while (opcion<'1' || opcion>'9');
		printf ("%c\n\n", opcion);
		if (raiz == NULL && opcion != '1' && opcion != '2' && opcion != '7' && opcion != '8' && opcion != '9')
			printf ("El Arbol esta vacio.\n");
		else switch (opcion)
		{
			case '1':
				printf ("Ingrese el entero: ");
                scanf ("%d", &entero); fflush(stdin);
					
				raiz = arbolInsertar (raiz, entero);
				printf ("\nEntero agregado correctamente.\n");
				break;
			case '2':
				arbolInsertarFile ();
				break;
			case '3': 
				arbolPreorden  (raiz, raiz); 
				break;
			case '4': 
				arbolInorden   (raiz, raiz); 
				break;
			case '5': 
				arbolPostorden (raiz, raiz); 
				break;
			case '6':
				printf ("\nArbol balanceado correctamente.\n");
				raiz = arbolBalancear (raiz);
				break;
			case '7':
				printf ("Altura    : %d\n", arbolAltura (raiz));
				printf ("Nodos     : %d\n", arbolNodos  (raiz));
				printf ("Completo  : %s\n", arbolCompleto   (raiz) ? "si" : "no");
				printf ("Balanceado: %s\n", arbolBalanceado (raiz) ? "si" : "no");
				break;
			case '8':
                 if (raiz != NULL)
                 {
                   printf ("Ingrese el entero que desea buscar: "); 
                   scanf("%d",&entero);
                   buscarArbol(raiz, entero);
                 } else
                   printf("El arbol esta vacio\n");
                   
		}
		if (opcion!='9')
		{
			putchar ('\n');
			system ("PAUSE");
		}
	} while (opcion!='9');
	return EXIT_SUCCESS;
}

void arbolInsertarFile()
{
	int var;
	
   if( (archivo=fopen(nombreDeArchivo,"r"))== NULL )
   {
   	printf("Error al abrir %s\n",nombreDeArchivo);
   }else
   { 
   	  while(!feof(archivo))
   	  {
   	  	fscanf(archivo,"%d;",&var);
   	  	raiz= arbolInsertar (raiz,var);
   	  }
   	  
   	  fclose(archivo);  
	  printf("Los datos fueron insertados correctamente\n\n");  
   }
}


Arbol arbolInsertar (Arbol nodo, int entero)
{
	if (nodo == NULL)
	{
		nodo = (Arbol) malloc (sizeof (struct AVLArbol));
		nodo->izq = NULL;
		nodo->der = NULL;
		nodo->entero = entero;
	}
	
	else if (entero < nodo->entero)
		nodo->izq = arbolInsertar (nodo->izq, entero);
	else if (entero > nodo->entero)
		nodo->der = arbolInsertar (nodo->der, entero);
		else
		{
			printf("Elemento %d fue previamente insertado.\n",entero);
		}
	return nodo;
}


void buscarArbol (Arbol nodo, int entero)
{
     if(nodo!=NULL)
     {
       if (entero < nodo->entero)
		buscarArbol(nodo->izq, entero);
	    else if (entero > nodo->entero)
		buscarArbol (nodo->der, entero);
		else if(entero == nodo->entero)
		printf("El elemento %d esta en el arbol",nodo->entero);
      }
      else
        printf("El elemento %d no esta en el arbol",entero);
}

Arbol arbolQuitarYRotar (Arbol nodo, int entero, Sentido rotacion)
{
	Arbol Arbolito;
	if (nodo != NULL)
	{
		if (entero == nodo->entero)
		{
			if (nodo->izq == NULL)
				Arbolito = nodo->der;
			else if (nodo->der == NULL)
				Arbolito = nodo->izq;
			else if (rotacion == IZQUIERDA)
			{
				for (Arbolito = nodo->izq; Arbolito->der != NULL; Arbolito = Arbolito->der);
				Arbolito->der = nodo->der;
				Arbolito = nodo->izq;
			} else {
				for (Arbolito = nodo->der; Arbolito->izq != NULL; Arbolito = Arbolito->izq);
				Arbolito->izq = nodo->izq;
				Arbolito = nodo->der;
			}
			free (nodo);
			nodo = Arbolito;
		}
		else if (entero < nodo->entero)
			nodo->izq = arbolQuitarYRotar (nodo->izq, entero, rotacion);
		else
			nodo->der = arbolQuitarYRotar (nodo->der, entero, rotacion);
	}
	return nodo;
}

void arbolPreorden (Arbol raiz, Arbol nodo)
{
	Arbol Arbolito;
	int hermanos = 0, borde1, borde2;
	Sentido direccion = CENTRO;
	if (nodo != NULL)
	{
		Arbolito = raiz;
		while (Arbolito!=nodo)
		{
			printf ("%c ",hermanos==2 && direccion == IZQUIERDA ? '\263': ' ');
			hermanos = 0;
			if (Arbolito->izq!=NULL)
				hermanos++;
			if (Arbolito->der!=NULL)
				hermanos++;
			if (nodo->entero < Arbolito->entero)
			{
				Arbolito = Arbolito->izq;
				direccion = IZQUIERDA;
			}
			else
			{
				Arbolito = Arbolito->der;
				direccion = DERECHA;
			}
		}
		borde1 = direccion == CENTRO ? '\304' :
			(hermanos == 1 || direccion == DERECHA ? '\300' : '\303');
		borde2 = nodo->izq != NULL || nodo->der != NULL ? '\302' : '\304';
		printf ("%c\304%c %d\n", borde1, borde2, nodo->entero);
		arbolPreorden (raiz, nodo->izq);
		arbolPreorden (raiz, nodo->der);
	}
}

void arbolInorden (Arbol raiz, Arbol nodo)
{
	Arbol Arbolito;
	Sentido direccion = CENTRO;
	int borde1, borde2;
	if (nodo != NULL)
	{
		arbolInorden (raiz, nodo->izq);
		Arbolito = raiz;
		while (Arbolito!=nodo)
			if (nodo->entero < Arbolito->entero)
			{
				printf ("%c ", direccion == DERECHA? '\263': ' ');
				Arbolito = Arbolito->izq;
				direccion = IZQUIERDA;
			}
			else
			{
				printf ("%c ", direccion == IZQUIERDA? '\263': ' ');
				Arbolito = Arbolito->der;
				direccion = DERECHA;
			}
		switch (direccion)
		{
			case IZQUIERDA: borde1 = '\332'; break;
			case CENTRO   : borde1 = '\304'; break;
			case DERECHA  : borde1 = '\300'; break;
		}
		if (nodo->izq!=NULL && nodo->der!=NULL)
			borde2 = '\305';
		else if (nodo->izq!=NULL)
			borde2 = '\301';
		else if (nodo->der!=NULL)
			borde2 = '\302';
		else
			borde2 = '\304';
		printf ("%c\304%c %d\n", borde1, borde2, nodo->entero);
		arbolInorden (raiz, nodo->der);
	}
}

void arbolPostorden (Arbol raiz, Arbol nodo)
{
	Arbol Arbolito;
	int hermanos = 0, borde1, borde2;
	Sentido direccion = CENTRO;
	if (nodo != NULL)
	{
		arbolPostorden (raiz, nodo->izq);
		arbolPostorden (raiz, nodo->der);
		Arbolito = raiz;
		while (Arbolito!=nodo)
		{
			printf ("%c ", hermanos==2 && direccion==DERECHA? '\263': ' ');
			hermanos = 0;
			if (Arbolito->izq!=NULL)
				hermanos++;
			if (Arbolito->der!=NULL)
				hermanos++;
			if (nodo->entero < Arbolito->entero)
			{
				Arbolito = Arbolito->izq;
				direccion = IZQUIERDA;
			}
			else
			{
				Arbolito = Arbolito->der;
				direccion = DERECHA;
			}
		}
		borde1 = direccion == CENTRO ? '\304' :
			(hermanos==1 || direccion == IZQUIERDA? '\332': '\303');
		borde2 = nodo->izq!=NULL || nodo->der!=NULL? '\301': '\304';
		printf ("%c\304%c %d\n", borde1, borde2, nodo->entero);
	}
}

int arbolAltura (Arbol nodo)
{
	int izq, der;
	if (nodo==NULL)
		return 0;
	izq = arbolAltura (nodo->izq);
	der = arbolAltura (nodo->der);
	return 1 + (izq > der ? izq : der);
}

Arbol arbolBalancear (Arbol nodo)
{
	int diferencia, entero;
	if (nodo != NULL)
	{
		nodo->izq = arbolBalancear (nodo->izq);
		nodo->der = arbolBalancear (nodo->der);
		diferencia = arbolAltura (nodo->izq) - arbolAltura (nodo->der);
		if (diferencia > 1 || diferencia < -1)
		{
			entero = nodo->entero;
			nodo = arbolBalancear (arbolInsertar (arbolQuitarYRotar (nodo, entero, diferencia > 1 ? DERECHA : IZQUIERDA), entero));
		}
	}
	return nodo;
}

int arbolNodos (Arbol nodo)
{
	return nodo==NULL ? 0 : 1 + arbolNodos (nodo->izq) + arbolNodos (nodo->der);
}

int arbolCompleto (Arbol nodo)
{
	return nodo==NULL || (	arbolNodos (nodo->izq) == arbolNodos (nodo->der) && arbolCompleto (nodo->izq) && arbolCompleto (nodo->der) );
}

int arbolBalanceado (Arbol nodo)
{
	int diferencia;
	if (nodo==NULL)
		return 1;
	diferencia = arbolAltura (nodo->izq) - arbolAltura (nodo->der);
	return diferencia >= -1 && diferencia <= 1 &&
		arbolBalanceado (nodo->izq) && arbolBalanceado (nodo->der);
}


