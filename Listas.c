#include <stdio.h>
#include <stdlib.h>

struct nodo
{
    int numero;
    struct nodo *sig;
};

typedef struct nodo node;

node *crearLista(node *inicio)
{
    inicio = NULL;
    return inicio;
}

void imprimir (node *lista)
{
     node *auxiliar;
     auxiliar = lista;
     
     while (auxiliar != NULL)
     {
           printf("%d \n",auxiliar->numero);
           auxiliar = auxiliar->sig;
     }
     
     return;
}

node *insertarEnLista(int numeroAInsertar, node *inicio)
{
    node *nuevoNodo;
    node *aux;
    
    nuevoNodo = (node *) malloc(sizeof(node));
    
    nuevoNodo->numero = numeroAInsertar;
    nuevoNodo->sig = NULL;
    
    if (inicio == NULL)
    {
        inicio = nuevoNodo;
    }
    else
    {
        aux = inicio;
        
        while (aux->sig != NULL)
        {
            aux = aux->sig;
        }
            
        aux->sig = nuevoNodo;
    }
    
    return inicio;
}

node *eliminarEnLista (int numeroAEliminar, node * apuntador)
{
     if (apuntador != NULL)
     {
                   
                   node *auxiliar;
                   node *anterior;
                   
                   auxiliar = apuntador;
                   anterior = NULL;
                   int borrar = 0;
                   
                   while (auxiliar != NULL)
                   {
                         
                         if (auxiliar->numero == numeroAEliminar)
                         {
                                              if (anterior == NULL)
                                              {
                                                  apuntador = apuntador->sig;
                                                  free(auxiliar);
                                                  borrar = 1;
                                                           
                                              }
                                              else if (auxiliar->sig != NULL)
                                              {
                                                 anterior->sig = auxiliar->sig;
                                                 free(auxiliar);
                                                 borrar = 1;
                                                 
                                              }
                                              else
                                              {
                                                  anterior->sig = NULL;
                                                  free(auxiliar);
                                                  borrar = 1;
                                                  
                                              }
                                              
                         }
                         
                         if (borrar == 1)
                         {
                             if (anterior == NULL)
                             {
                                auxiliar = apuntador;          
                             }
                             else
                             {
                                 auxiliar = anterior->sig;
                             }     
                         }
                         else
                         {
                          anterior = auxiliar;
                          auxiliar = auxiliar->sig;
                         }
                         
                         borrar = 0;
                         
                         
                         
                   }
                   
                   
     }
     
     return apuntador;
     
}
 

int main()
{
    
    node *lista;
    
    lista = crearLista (lista);
    
    lista = insertarEnLista(3, lista);
    lista = insertarEnLista(4, lista);
    lista = insertarEnLista(8, lista);
    lista = insertarEnLista(3, lista);
    lista = insertarEnLista(2, lista);
    
    imprimir(lista);
    
    printf("////// \n");
    
    lista = eliminarEnLista(4, lista);
    imprimir(lista);
    
    
  
  system("PAUSE");	
  return 0;
}
