#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct _nodo {
   int indice;
   struct _nodo *siguiente;
   struct _nodo *anterior;
   char palabra[30];
} tipoNodo;

typedef tipoNodo *pNodo;
typedef tipoNodo *Lista;

void Insertar(Lista *l, int v, char p[30]);

void Insertar(Lista *lista, int v, char p[30])
{
   pNodo nuevo, actual;

   nuevo = (pNodo)malloc(sizeof(tipoNodo));
   nuevo->indice = v;
   
   actual = *lista;
   if(actual) while(actual->anterior) actual = actual->anterior;
   if(!actual || actual->indice > v) 
   {
      nuevo->siguiente = actual; 
      nuevo->anterior = NULL;
      if(actual) actual->anterior = nuevo;
      if(!*lista) *lista = nuevo;
   }
   else 
   {
      while(actual->siguiente && actual->siguiente->indice <= v) 
         actual = actual->siguiente;
      	 nuevo->siguiente = actual->siguiente;
      	 actual->siguiente = nuevo;
      	 nuevo->anterior = actual;
      if(nuevo->siguiente) nuevo->siguiente->anterior = nuevo;
   }
   strcpy(nuevo->palabra,p);     
}
