/* ESCUELA DE INGENIERIA INFORMATICA
PRIMER PROYECTO DE ALGORTIMOS Y PROGRAMACION II
EL AHORCADO - VERSION UCAB

GISELLE MINGUE
CARHIL FUENTES

*/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <conio.h>
#include <string.h>
#include<process.h>
#include "dobleenlace.c"

#define VALIDO "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

void imprimirAhorcado(char frase[30]);
void juego(Lista *l,  int cantidad);
int cargarTXT(Lista *l);


int oportunidades;
int palabraActual;
int cantidad;


int main ( )
{
	Lista pplista=NULL;
	pNodo ppNodo=NULL;
    int opcion;
	
	printf ("Bienvenido al juego El Ahorcado desarrollado por estudiantes de la UCAB\n");
	do{
			oportunidades = 5;
	        printf ("\n");
	        printf ("1.- Instrucciones del juego\n\n");
	        printf ("2.- Jugar\n\n");
	        printf ("0.- Salir\n\n");
	        printf ("\n");
	        printf ("Elija una opcion: ");
	        scanf  ("%d",&opcion);
	        printf ("\n");
	            if ((opcion > 2) || (opcion < 0))
	                   printf ("Error, debe indicar la opcion correcta\n\n");
	                   
	            else       
			        switch ( opcion )
			        {
			                case 1: 
			                     printf ("**Instrucciones del juego**\n\n");
			                     printf ("EL AHORCADO juego dinamico y de manejo de destrezas que cosiste en adivinar\n");
								 printf ("las letras que componen una palabra con la finalidad de obtener esta completa,\n"); 
								 printf ("dispones de 5 vidas de las cuales posteriormente seran restadas por cada\n");
								 printf ("equivocacion cometida, ademas hay un minimo de 3 palabras en un rango de 10.");
								 printf ("\n\n");
			                     break;                         
			                case 2:
			                     printf ("\n");
			                     do{
			                     
				                     printf ("Con cuantas palabras desea jugar? (3 - 12): ");
				                     scanf  ("%d",&cantidad);
			                     
			                     
				                     if ((cantidad < 3) || (cantidad > 12))
				                     {
		                   	     		printf ("Error, debe indicar una cantidad entre 3 y 12 palabras.\n\n");
		                   	     	 }
		                   			 else
										{		
				                             cargarTXT(&pplista);
				                     		 juego(&pplista ,  cantidad); 
				                        }
									 break;	
									 
							     }while ((cantidad < 3) && (cantidad > 12));
			        }
		}while (opcion != 0);
    system ("PAUSE");	
    return 0;
}




void juego(Lista *l, int cantidad)
{
	int i=0;
    pNodo actual;
	actual= *l;	 
    while(actual->anterior) actual = actual->anterior;
    while(actual && i < cantidad && oportunidades>0) 
	{
	     imprimirAhorcado(actual->palabra);
	     if(oportunidades>0);
         actual = actual->siguiente;
         i++;
    }
}

int cargarTXT(Lista *l)
{
      FILE *fichero;
      fichero = fopen("G:\\ProyectoListo\\Archivo.txt","r");
      if (fichero == NULL){
	      printf("No se pudo abrir el archivo");
	      return 0;
      }    
      char *palabra, *indice;
     char lineas[100]; 

    while (fscanf(fichero, "%s", lineas) == 1 )
	{
      palabra = strtok(lineas, ";" );
      
      if(palabra != NULL)
	  {    
         indice = strtok(NULL, "\\n");
         int temp= atoi(indice);
         Insertar(l,temp,palabra); 
      }
    }
   fclose(fichero);
   Lista n= *l;
   system("PAUSE");
}

int caracterInvalido(char pal)
{
	int Giselle;
	int bol=0;
	  for (Giselle=0;Giselle<strlen(VALIDO);Giselle++)
	  {
	  	if (pal == VALIDO[Giselle])
	  	{
			bol=1;
			break;
		}
	  }
	  return bol;
}

void imprimirAhorcado(char frase[60]) 
{
    char rep[100],temporal[100];
    char pal;
    int longitud,i=0,j,inicial,acertado=0,temp=0;
    int repetido=0,gano=0;
    strcpy(rep," "); 
    strcpy(temporal," "); 
    system("cls");
    pal=' ';
    longitud = 0;
    inicial = 0;
    j = 0;
   
    rep[0] = ' ';
    rep[1] = '\0';
   
   
    do {
       system("cls");
       
        temp=0;
   
        if(inicial == 0) {
         for(i=0;i<strlen(frase);i++)
		  {
          if(frase[i] == ' ') 
		  {
            temporal[i] = ' ';
             longitud++;
          }
          else 
		  {
             temporal[i] = '_';       
             longitud++;
          }
         }
        }
   
        inicial = 1;
       
        temporal[longitud] = '\0';
       
        for(i=0;i<j;i++) 
		{
           if(rep[i] == pal ) 
		   {
            repetido = 1;
            break;
          }
          else 
		  {
           repetido = 0;
         }
        }
       
        if(repetido == 0) {
         for(i=0;i<strlen(frase);i++) 
		 {
                    if(frase[i] == pal) 
			{
             temporal[i] = pal;
              acertado++;
              temp=1;
            }
          }
        }
        
        
       if (caracterInvalido(pal) == 1)
        {
        	
		        if((repetido == 0) && (temp == 0))
				 {
		           oportunidades = oportunidades - 1;
		         }
		        
		        else if(repetido == 1) 
				{
		         printf("Ya se ha introducido este caracter.");
		         printf("\n\n");
		        }
		        
  		 }
  		 else if (pal != ' ')
  		 {
  		 	printf("El caracter es invalido, solo se admiten mayusculas y no\n");
			printf("esta permitido el uso de numeros o simbolos especiales.\n");
  		 	system("PAUSE");
  		 }
        printf("\n");
       
        for(i=0;i<strlen(temporal);i++) 
		{
         printf(" %c ",temporal[i]);
        }
       
        printf("\n");
       
        if(strcmp(frase,temporal) == 0) 
		{
            gano = 1;
            break;
        }
        
        printf("\n");
        printf("Letras Acertadas: %d\n\n",acertado);
        printf("Vidas Restantes: ");
        for(int k=0; k<oportunidades;k++) printf("\003 ");
        printf("\n");
   
        rep[j] = pal;
        j++;
       
        if (oportunidades==0)
        {
           break;
        }
        printf("\n");
        printf("Introduzca una letra: \n");
        scanf("\n%c",&pal); 
		fflush(stdin);
      
    }while(oportunidades != 0);
   
   
    if(gano) 
	{
                printf("\n\n");
        printf("Felicidades, adivinaste.");
    }
    else 
	{
                printf("\n\n");
        printf("Has perdido.");
    }
   
    printf("\n\n");
    system("PAUSE");
} 
