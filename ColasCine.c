#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define entrada 90.45

struct persona
{
    char nombre[30];
    int cantidad;
    struct persona *siguiente;
};

struct boleto
{
    char nombre[30];
    int cantidad;
    float monto;
    struct boleto *siguiente;
};

typedef struct persona nodoPersona;
typedef struct boleto nodoBoleto;

nodoBoleto *taquilla;

nodoPersona *crearCola(nodoPersona *cola)
{
    cola = NULL;
    return cola;
}

nodoBoleto *crearLista(nodoBoleto *lista)
{
    lista = NULL;
    return lista;
}

void imprimir (nodoPersona *cola)
{
     nodoPersona *auxiliar;
     auxiliar = cola;
     
     while (auxiliar != NULL)
     {
           printf("%s \n",auxiliar->nombre);
           auxiliar = auxiliar->siguiente;
     }
     
     return;
}

void imprimirLista (nodoBoleto *lista)
{
     nodoBoleto *auxiliar;
     auxiliar = lista;
     
     while (auxiliar != NULL)
     {
           printf("Persona: %s \n",auxiliar->nombre);
           printf("Cantidad de Boletos: %d \n",auxiliar->cantidad);
           printf("Monto: %10.2f Bs. \n",auxiliar->monto);
           auxiliar = auxiliar->siguiente;
     }
     
     return;
}

nodoPersona *encolar(char nombreAInsertar[30], int cantidad, nodoPersona *cola)
{
    nodoPersona *nuevoNodo;
    nodoPersona *auxiliar;
    
    nuevoNodo = (nodoPersona *) malloc(sizeof(nodoPersona));
    
    strcpy(nuevoNodo->nombre,nombreAInsertar);
    nuevoNodo->cantidad = cantidad;
    nuevoNodo->siguiente = NULL;
    
    if (cola == NULL)
    {
        cola = nuevoNodo;
    }
    else
    {
        auxiliar = cola;
        
        while (auxiliar->siguiente != NULL)
        {
            auxiliar = auxiliar->siguiente;
        }
            
        auxiliar->siguiente = nuevoNodo;
    }
    
    return cola;
}

void venderBoleto(char nombre[30], int cantidad)
{
    nodoBoleto *nuevoNodo;
    nodoBoleto *auxiliar;
    
    nuevoNodo = (nodoBoleto *) malloc(sizeof(nodoBoleto));
    
    strcpy(nuevoNodo->nombre,nombre);
    nuevoNodo->cantidad = cantidad;
    nuevoNodo->monto = cantidad*entrada;
    nuevoNodo->siguiente = NULL;
    
    if (taquilla == NULL)
    {
        taquilla = nuevoNodo;
    }
    else
    {
        auxiliar = taquilla;
        
        while (auxiliar->siguiente != NULL)
        {
            auxiliar = auxiliar->siguiente;
        }
            
        auxiliar->siguiente = nuevoNodo;
    }
    
    return;
}

nodoPersona *desencolar (nodoPersona *cola)
{
     if (cola != NULL)
     {
                   
                   nodoPersona *auxiliar;
                   
                   auxiliar = cola;
                   
                   venderBoleto(auxiliar->nombre, auxiliar->cantidad);
                   
                   cola = cola->siguiente;
                   
                   free (auxiliar);
     }
     
     return cola;
     
}
 

int main()
{
    
    nodoPersona *cola;
    
    cola = crearCola (cola);
    taquilla = crearLista(taquilla);
    
    cola = encolar("Jose", 2, cola);
    cola = encolar("Juan", 3, cola);
    cola = encolar("Ana", 3, cola);
    cola = encolar("Gabriel", 1, cola);
    cola = encolar("Vanessa", 2, cola);
    
    printf("////// Personas en cola \n");
    imprimir(cola);
    printf("////// Ventas taquilla \n");
    imprimirLista(taquilla);
    
    printf("\n");
    printf("////// \n");
    
    while (cola!=NULL)
    {
     cola = desencolar(cola);
    }
    printf("////// Personas en cola \n");
    imprimir(cola);
    printf("////// Ventas taquilla \n");
    imprimirLista(taquilla);
    
    
  
  system("PAUSE");	
  return 0;
}
