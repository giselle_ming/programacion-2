#include <stdio.h>
#include <stdlib.h>
#define dosMilDies 2010

struct nodo
{
    int year;
    char autor[30];
    char editorial[30];
    char titulo[50];
    struct nodo *sig;
};

typedef struct nodo node;

node *crearPila(node *pila)
{
    pila = NULL;
    return pila;
}

void imprimir (node *pila)
{
     node *auxiliar;
     auxiliar = pila;
     
     while (auxiliar != NULL)
     {
           printf("%s \n",auxiliar->titulo);
           auxiliar = auxiliar->sig;
     }
     
     return;
}

node *apilar(int year, char autor[30], char editorial[30], char titulo[50], node *pila)
{
  node *nuevo = (node*) malloc (sizeof(node));
  nuevo->year = year;
  strcpy(nuevo->autor,autor);
  strcpy(nuevo->editorial,editorial);
  strcpy(nuevo->titulo,titulo);
  nuevo->sig = pila;
  
  pila = nuevo;
    
  return pila;
}

node *desapilar (node *pila)
{
     node *auxiliar = pila;
     pila = auxiliar->sig;
     
     free(auxiliar);    
     
     return pila;
     
}

int main()
{
    
    node *pila;
    node *pila2;
    
    pila = crearPila(pila);
    pila2 = crearPila(pila2);
    
    
    pila = apilar(2010,"Er conde Bond", "Guacharo", "Er Conde", pila);
    pila = apilar(2014,"Pepito", "Pepe", "Peluche", pila);
    pila = apilar(2010,"las aventuras de rin tin tin", "Grillo", "Grillo", pila);
    pila = apilar(2014,"El chavo", "8 productions", "Chespirito", pila);
    
    
    printf ("----Pila 1 ---- \n");
    imprimir(pila);
    printf ("----Pila 2 ---- \n");
    imprimir(pila2);
    
    printf ("////////// \n");
    
    while (pila!=NULL)
    {
          if (pila->year == dosMilDies)
          {
             pila2 = apilar(pila->year, pila->autor, pila->editorial, pila->titulo, pila2);
             pila = desapilar(pila);
          }
          else
          {
              pila = desapilar(pila);
          }
          
    }
    
    
    printf ("////////// \n");
    
    
    printf ("----Pila 1 ---- \n");
    imprimir(pila);
    printf ("----Pila 2 ---- \n");
    imprimir(pila2);
    
    
  
  system("PAUSE");	
  return 0;
}
